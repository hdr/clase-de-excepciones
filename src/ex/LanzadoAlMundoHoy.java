package ex;

public class LanzadoAlMundoHoy {

	// Suma entre 0 y n
	public static int sumar(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("número negativo: " +  n);
		}
		
		if (n == 0) {
			return 0;
		}

		return n + sumar(n - 1);
	}

	// a ** b
	public static int potencia(int a, int b) {
		if (b < 0) {
			throw new IllegalArgumentException("exponente no puede ser negativo: " + b);
		}
		
		if (a == 0 && b == 0) {
			throw new RuntimeException("0 a la 0 es indefinido");
		}
		
		if (b <= 1) {
			return 1;
		}

		return a * potencia(a, b - 1);
	}

	// Suma entre a y b
	public static int sumaEntre(int a, int b) {
		if (a > b) {
			throw new IllegalArgumentException("intervalo inválido: " + a + " > " + b);
		}
		
		if (a == b) {
			return a;
		}

		return a + sumaEntre(a + 1, b);
	}

	public static void main(String[] args) {
		int n = -36;
		// Acá se deberían pedir los datos a le usuarie.

		try {
			System.out.println("sumar(" + n + ") = " + sumar(n));
		} catch (IllegalArgumentException e) {
			// Acá se volvería a pedir los datos a le usuarie
		}

		int a = 2;
		int b = -3;
		// Acá se deberían pedir los datos a le usuarie.

		try {
			System.out.println("potencia(" + a + ", " + b + ") = " + potencia(a, b));
		} catch (IllegalArgumentException e) {
			System.out.println("IllegalArgumentException");
			// Acá se volvería a pedir los datos a le usuarie
		} catch (RuntimeException e) {
			System.out.println("RuntimeException");
			// Acá se volvería a pedir los datos a le usuarie
		}

		a = 15;
		b = 7;
		// Acá se deberían pedir los datos a le usuarie.

		try {
			System.out.println("sumaEntre(" + a + ", " + b + ") = " + sumaEntre(a, b));
		} catch (IllegalArgumentException e) {
			// Acá se volvería a pedir los datos a le usuarie
		}

	}

}
