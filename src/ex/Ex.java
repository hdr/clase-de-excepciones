package ex;

public class Ex {

	public static char caracterMasRepetido(String s) {
		if (s.length() == 0) {
			throw new RuntimeException("string vacío, longitud: " + s.length());
		}
		
		char r = s.charAt(0);

		for (int i = 1; i < s.length(); i++) {
			if (cantidadDeApariciones(s, r) < cantidadDeApariciones(s, s.charAt(i))) {
				r = s.charAt(i);
			}
		}

		return r;
	}

	public static int cantidadDeApariciones(String s, char c) {
		if (s.length() == 0) {
			return 0;
		}

		// aclaración "s.substring(1) == resto(s)"
		return (s.charAt(0) == c ? 1 : 0) + cantidadDeApariciones(s.substring(1), c);
	}

	public static void main(String[] args) {
		String s = "";
		
		System.out.println("hago cosas antes del try/catch");
		
		try {
			System.out.println("El caracter más repetido en " + s + " es '" + caracterMasRepetido(s) + "'.");
		} catch (RuntimeException e) {
			System.out.println("Disculpe estimade usuarie, ingresó una palabra sin letras.");
		}
		
		System.out.println("hago cosas después del try/catch");
	}
	
}
